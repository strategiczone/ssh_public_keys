#!/usr/bin/env bash
#title		    : install_pubkey_gabriel.sh
#description	: Installation de la cle public de Gabriel SCI
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 20181210
#version	    : 0.2
#notes	    	:
#===================

mkdir -p ~/.ssh
curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_rsa_raphael_lalung.pub | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys

